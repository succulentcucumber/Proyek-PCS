﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace WindowsFormsApp1
{
    public partial class formPegawai_add : Form
    {
        public formPegawai_add()
        {
            InitializeComponent();
        }

        private void formPegawai_add_Load(object sender, EventArgs e)
        {
            Form1 parent = (Form1)this.MdiParent;
            OracleCommand cmd = new OracleCommand("SELECT NAMA_HOTEL FROM CABANG_HOTEL WHERE ID_HOTEL='"+parent.cabang+"'",parent.oc);
            textBox2.Text = cmd.ExecuteScalar().ToString();
            Dock = DockStyle.Fill;
            OracleDataAdapter oda = new OracleDataAdapter("SELECT * FROM JABATAN",parent.oc);
            DataTable jabatan = new DataTable();
            oda.Fill(jabatan);
            comboBox1.DataSource = jabatan;
            comboBox1.DisplayMember = "NAMA_JABATAN";
            comboBox1.ValueMember = "ID_JABATAN";
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            textBox1.Text = "";
            Form1 parent = (Form1)this.MdiParent;
            String nama = textBox3.Text;
            if(nama.Length > 1)
            {
                OracleCommand cmd = new OracleCommand("SELECT AUTO_GEN_ID_PEGAWAI('"+nama+"') FROM DUAL",parent.oc);
                textBox1.Text = cmd.ExecuteScalar().ToString();

                textBox5.Text = textBox1.Text;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox6.Text != "")
            {
                Form1 parent = (Form1)this.MdiParent;
                String id = textBox1.Text.ToUpper();
                String nama = textBox3.Text.ToUpper();
                String telp = textBox4.Text;
                String jabatan = comboBox1.SelectedValue.ToString();
                String day = dateTimePicker1.Value.Day.ToString();
                String mon = dateTimePicker1.Value.Month.ToString();
                String year = dateTimePicker1.Value.Year.ToString();

                if (nama.Length < 2 || telp.Length > 13 || telp.Length < 10)
                {
                    MessageBox.Show("Terdapat data tidak valid!");
                }
                else
                {
                    string encrypted_pass = parent.MD5Hash(textBox6.Text);
                    OracleCommand cmd = new OracleCommand("INSERT INTO PEGAWAI VALUES('" + id + "','" + jabatan + "','" + parent.cabang + "','" + nama + "',TO_DATE(LPAD('" + day + "',2,'0')||'/'||LPAD('" + mon + "',2,'0')||'/'||LPAD('" + year + "',4,'0'),'DD/MM/YYYY'),'" + telp + "','"+encrypted_pass+"')", parent.oc);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Register Sukses!");
                }
            }
            else
            {
                MessageBox.Show("Password tidak boleh kosong!");
            }
            

        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox3.Text = "";
            dateTimePicker1.Value = DateTime.Now;
            textBox4.Text = "";
            comboBox1.SelectedIndex = 0;
        }
    }
}
