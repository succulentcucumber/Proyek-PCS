﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class formInvoice : Form
    {
        string bookingID = "";
        string guest = "";
        string checkin = "";
        string checkout = "";
        string noRoom = "";
        string tipe = "";
        string qtyRoom = "";
        string subtotalRoom = "";
        string subtotalExtra = "";
        string total = "";
        
        public formInvoice(string bookId,string guestName,string cin,string cout,string roomNo,string type,string totRoom,string subRoom,string subExtra, string totHarga)
        {
            InitializeComponent();

            bookingID = bookId;
            guest = guestName;
            checkin = cin;
            checkout = cout;
            noRoom = roomNo;
            tipe = type;
            qtyRoom = totRoom;
            subtotalRoom = subRoom;
            subtotalExtra = subExtra;
            total = totHarga;
        }

        private void formInvoice_Load(object sender, EventArgs e)
        {
            invoice invo = new invoice();
            invoice_extra invo_extra = new invoice_extra();
            invo_extra.SetDatabaseLogon("pcs_proyek", "5521");
            invo.SetDatabaseLogon("pcs_proyek", "5521");
            invo_extra.SetParameterValue("idBooking", bookingID);
            invo.SetParameterValue("bookingNo", bookingID);
            invo.SetParameterValue("guestName", guest);
            invo.SetParameterValue("cin", checkin);
            invo.SetParameterValue("cout", checkout);
            invo.SetParameterValue("roomNo", noRoom);
            invo.SetParameterValue("tipe", tipe);
            invo.SetParameterValue("totalRoom", qtyRoom);
            invo.SetParameterValue("subtotalKamar", subtotalRoom);
            invo.SetParameterValue("subtotalExtra", subtotalExtra);
            invo.SetParameterValue("total", total);
            crystalReportViewer1.ReportSource = invo;
            crystalReportViewer1.Refresh();

        }
    }
}
