﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client;
using System.Security.Cryptography;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {//tess
        public OracleConnection oc;
        public String cabang;
        public Form currentForm = null;

        

        public Form1()
        {
            InitializeComponent();
            formLogin login = new formLogin();
            login.MdiParent = this;
            login.Show();
            cabang = "null";
            //connection ivan
            try
            {
                oc = new OracleConnection("user id=pcs_proyek;password=5521;data source = orcl");
                oc.Open();
                MessageBox.Show("Connection Open Ivan");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Fail Ivan : "+ex.Message);
                try
                {
                    oc = new OracleConnection("user id=n217116635;password=217116635;data source = (DESCRIPTION =(ADDRESS_LIST =(ADDRESS = (PROTOCOL = TCP)(HOST = localhost)(PORT = 1521)))(CONNECT_DATA =(SERVICE_NAME = admin)))");
                    oc.Open();
                    MessageBox.Show("Connection Open Ming");
                }
                catch (Exception ex1)
                {
                    MessageBox.Show("Fail Ming : " + ex1.Message);
                }
            }
            //connection ming
            
        }

        public string MD5Hash(string text)
        {
            MD5 md5 = new MD5CryptoServiceProvider();

            md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(text));

            byte[] result = md5.Hash;

            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                strBuilder.Append(result[i].ToString("x2"));
            }

            return strBuilder.ToString();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        public void showMenuStrip(string jabatan)
        {
            if(jabatan == "ADMIN"||jabatan=="MANAGER")
            {
                ToolStripMenuItem logout = new ToolStripMenuItem();
                logout.Text = "Log Out";
                logout.Click += logout_Click;

                ToolStripMenuItem reserv = new ToolStripMenuItem();
                reserv.Text = "Reservation Center";

                ToolStripMenuItem reserv_add = new ToolStripMenuItem();
                reserv_add.Text = "Add";
                reserv_add.Click += reserv_add_Click;
                reserv.DropDownItems.Add(reserv_add);

                ToolStripMenuItem reserv_edit = new ToolStripMenuItem();
                reserv_edit.Text = "Edit";
                reserv_edit.Click += reserv_edit_Click;
                reserv.DropDownItems.Add(reserv_edit);

                ToolStripMenuItem checkout = new ToolStripMenuItem();
                checkout.Text = "Checkout";
                checkout.Click += checkout_Click;
                reserv.DropDownItems.Add(checkout);

                ToolStripMenuItem admin = new ToolStripMenuItem();
                admin.Text = "Adminstration";

                ToolStripMenuItem admin_pegawai = new ToolStripMenuItem();
                ToolStripMenuItem admin_kamar = new ToolStripMenuItem();
                admin_kamar.Text = "Kamar Hotel";
                admin_pegawai.Text = "Pegawai";
                admin.DropDownItems.Add(admin_kamar);
                admin.DropDownItems.Add(admin_pegawai);

                ToolStripMenuItem admin_pegawai_add = new ToolStripMenuItem();
                admin_pegawai_add.Text = "Register";
                admin_pegawai_add.Click += admin_pegawai_add_Click;
                ToolStripMenuItem admin_pegawai_edit = new ToolStripMenuItem();
                admin_pegawai_edit.Text = "Edit";
                admin_pegawai_edit.Click += admin_pegawai_edit_Click;
                admin_pegawai.DropDownItems.Add(admin_pegawai_add);
                admin_pegawai.DropDownItems.Add(admin_pegawai_edit);

                ToolStripMenuItem admin_kamar_add = new ToolStripMenuItem();
                admin_kamar_add.Text = "Add";
                admin_kamar_add.Click += admin_kamar_add_Click;
                ToolStripMenuItem admin_kamar_edit = new ToolStripMenuItem();
                admin_kamar_edit.Text = "Edit";
                admin_kamar_edit.Click += admin_kamar_edit_Click;
                admin_kamar.DropDownItems.Add(admin_kamar_add);
                admin_kamar.DropDownItems.Add(admin_kamar_edit);

                ToolStripMenuItem laporan = new ToolStripMenuItem();
                laporan.Text = "Report";
                laporan.Click += laporan_Click;

                menuStrip1.Items.Add(reserv);
                menuStrip1.Items.Add(admin);
                menuStrip1.Items.Add(laporan);
                menuStrip1.Items.Add(logout);
                

            }
            else if (jabatan == "RESEPSIONIS")
            {
                ToolStripMenuItem logout = new ToolStripMenuItem();
                logout.Text = "Log Out";
                logout.Click += logout_Click;

                ToolStripMenuItem reserv = new ToolStripMenuItem();
                reserv.Text = "Reservation Center";

                ToolStripMenuItem reserv_add = new ToolStripMenuItem();
                reserv_add.Text = "Add";
                reserv_add.Click += reserv_add_Click;
                reserv.DropDownItems.Add(reserv_add);

                ToolStripMenuItem reserv_edit = new ToolStripMenuItem();
                reserv_edit.Text = "Edit";
                reserv_edit.Click += reserv_edit_Click;
                reserv.DropDownItems.Add(reserv_edit);

                ToolStripMenuItem checkout = new ToolStripMenuItem();
                checkout.Text = "Checkout";
                checkout.Click += checkout_Click;
                reserv.DropDownItems.Add(checkout);

                menuStrip1.Items.Add(reserv);
                menuStrip1.Items.Add(logout);
            }
        }
        private void laporan_Click(object sender, EventArgs e)
        {
            formLaporan fPegawai_edit = new formLaporan();
            fPegawai_edit.MdiParent = this;
            fPegawai_edit.Show();
            if (currentForm != null)
            {
                currentForm.Close();
            }
            currentForm = fPegawai_edit;
        }

        private void admin_pegawai_edit_Click(object sender, EventArgs e)
        {
            formPegawai_edit fPegawai_edit = new formPegawai_edit();
            fPegawai_edit.MdiParent = this;
            fPegawai_edit.Show();
            if (currentForm != null)
            {
                currentForm.Close();
            }
            currentForm = fPegawai_edit;
        }

        private void admin_kamar_add_Click(object sender, EventArgs e)
        {
            formKamar_add fKamar_add = new formKamar_add();
            fKamar_add.MdiParent = this;
            fKamar_add.Show();
            if (currentForm != null)
            {
                currentForm.Close();
            }
            currentForm = fKamar_add;
        }

        private void admin_kamar_edit_Click(object sender, EventArgs e)
        {
            formKamar_edit fKamar_edit = new formKamar_edit();
            fKamar_edit.MdiParent = this;
            fKamar_edit.Show();
            if(currentForm != null)
            {
                currentForm.Close();
            }
            currentForm = fKamar_edit;
        }

        private void admin_pegawai_add_Click(object sender, EventArgs e)
        {
            formPegawai_add fPegawai_add = new formPegawai_add();
            fPegawai_add.MdiParent = this;
            fPegawai_add.Show();
            if(currentForm != null)
            {
                currentForm.Close();
            }
            currentForm = fPegawai_add;
        }

        public void logout_Click(object sender, EventArgs e)
        {
            formLogin login = new formLogin();
            login.MdiParent = this;
            login.Show();
            menuStrip1.Items.Clear();
            if(currentForm!=null)
                currentForm.Close();
        }

        public void reserv_add_Click(object sender, EventArgs e)
        {
            formReservation_add reser = new formReservation_add();
            reser.MdiParent = this;
            reser.Show();
            if(currentForm != null)
            {
                currentForm.Close();
            }
            
            currentForm = reser;
        }

        public void reserv_edit_Click(object sender, EventArgs e)
        {
            formReservation_edit reser = new formReservation_edit();
            reser.MdiParent = this;
            reser.Show();
            if (currentForm != null)
            {
                currentForm.Close();
            }

            currentForm = reser;
        }

        public void checkout_Click(object sender, EventArgs e)
        {
            formReservation_CheckOut reser = new formReservation_CheckOut();
            reser.MdiParent = this;
            reser.Show();
            if (currentForm != null)
            {
                currentForm.Close();
            }

            currentForm = reser;
        }
    }
}
