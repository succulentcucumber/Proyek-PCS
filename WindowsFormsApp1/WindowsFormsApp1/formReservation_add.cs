﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace WindowsFormsApp1
{
    public partial class formReservation_add : Form
    {
        formLogin fl = new formLogin();
        Form1 parent;
        string id_booking = "booking7";
        int total = 0;

        List<string> listNik = new List<string>();
        List<string> listNama = new List<string>();
        List<string> listEmail = new List<string>();
        List<string> listHp = new List<string>();
        List<int> listTipeKamar = new List<int>();
        List<int> listNoKamar = new List<int>();
        List<string> listExtraFas = new List<string>();
        List<string> listTotal = new List<string>();
        public formReservation_add()
        {
            InitializeComponent();
            this.Size = fl.Size;
            FormBorderStyle = FormBorderStyle.None;
            
        }

        private void formReservation_Load(object sender, EventArgs e)
        {
            Dock = DockStyle.Fill;
            parent = (Form1)this.MdiParent;
            refreshData();
            button6.Hide();
            button7.Hide();
            groupBox2.Enabled = false;
            groupBox3.Enabled = false;
            groupBox4.Enabled = false;
            panel1.Enabled = false;
            
            try
            {
                OracleDataAdapter oda = new OracleDataAdapter("select kode_tipe,jenis_kamar from tipe_kamar", parent.oc);
                DataTable dt = new DataTable();
                oda.Fill(dt);
                comboBox1.ValueMember = "kode_tipe";
                comboBox1.DisplayMember = "jenis_kamar";
                comboBox1.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show("cbox1 : " + ex.Message);
            }

            try
            {
                OracleCommand cmd = new OracleCommand("select nama_hotel from cabang_hotel where id_hotel = '" + parent.cabang + "'", parent.oc);
                label14.Text = cmd.ExecuteScalar() + "";
            }
            catch (Exception ex)
            {
                MessageBox.Show("cabang"+ex.Message);
            }
            listBox2.DisplayMember = "nama_fasilitas";
            listBox2.ValueMember = "id_fasilitas";

            try
            {
                OracleCommand cmd = new OracleCommand("select auto_gen_id_booking('" + parent.cabang + "') from dual", parent.oc);
                textBox1.Text = cmd.ExecuteScalar() + "";
                id_booking = textBox1.Text;
            }
            catch (Exception ex)
            {
                MessageBox.Show("generateid"+ex.Message);
            }
        }

        private void refreshData()
        {
            try
            {
                OracleDataAdapter oda = new OracleDataAdapter("SELECT K.NOMOR_KAMAR as NOMOR FROM KAMAR_HOTEL K WHERE K.KODE_TIPE = '"+comboBox1.SelectedValue+"' AND K.ID_HOTEL = '"+parent.cabang+"' AND K.ID_KAMAR NOT IN( SELECT DISTINCT D.ID_KAMAR FROM DBOOKING D, BOOKING B WHERE D.ID_BOOKING = B.ID_BOOKING AND TO_CHAR(B.WAKTU_CIN, 'DD/MM/YYYY') >= '"+dateTimePicker1.Value.ToString("dd/MM/yyyy")+ "' AND TO_CHAR(B.WAKTU_COUT,'DD/MM/YYYY') <= '" + dateTimePicker2.Value.ToString("dd/MM/yyyy") + "') ORDER BY 1", parent.oc);
                DataTable dt = new DataTable();
                oda.Fill(dt);
                dataGridView1.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show("dgv1 : " + ex.Message);
            }

            try
            {
                listBox1.Items.Clear();
                if(listBox1.Items.Count <1)
                {
                    listBox1.Items.Clear();
                }

                OracleDataAdapter oda = new OracleDataAdapter("select nama_fasilitas,id_fasilitas from fasilitas where addable='YES'", parent.oc);
                DataTable dt = new DataTable();
                oda.Fill(dt);

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    listBox1.Items.Add(dt.Rows[i][0]);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("extra" + ex.Message);
            }

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            refreshData();
            dataGridView2.DataSource = null;
            label10.Text = "";

            try
            {
                OracleCommand cmd = new OracleCommand("select harga_kamar from tipe_kamar where kode_tipe = '" + comboBox1.SelectedValue + "'", parent.oc);
                label15.Text = cmd.ExecuteScalar()+"";
            }
            catch (Exception ex)
            {
                MessageBox.Show("harga kamar"+ex.Message);
            }

            refreshHarga();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                label10.Text = dataGridView1[0, e.RowIndex].Value + "";
                refreshRoomFacility();
            }
            catch (Exception)
            {

            }
        }
        
        private void refreshRoomFacility()
        {
            try
            {
                OracleDataAdapter oda = new OracleDataAdapter("select f.nama_fasilitas,f.deskripsi from fasilitas f, isi_fasilitas if where if.kode_tipe = '" + comboBox1.SelectedValue + "' and f.id_fasilitas = if.id_fasilitas", parent.oc);
                DataTable dt = new DataTable();
                oda.Fill(dt);
                dataGridView2.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show("dgv2 : " + ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            listBox2.Items.Add(listBox1.Text);
            refreshSubtotal();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                listBox2.Items.RemoveAt(listBox2.SelectedIndex);
            }
            catch (Exception)
            {
                
            }
            refreshSubtotal();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            listTipeKamar.Add(comboBox1.SelectedIndex);
            listNoKamar.Add(dataGridView1.CurrentCell.RowIndex);
            listTotal.Add(textBox2.Text);

            listBox3.Items.Add(label10.Text);
        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
            refreshData();
            refreshHarga();
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            refreshData();
            refreshHarga();
        }
        
        private void refreshHarga()
        {
            listBox2.Items.Clear();
            TimeSpan t = dateTimePicker2.Value - dateTimePicker1.Value;
            double jum_hari = t.TotalDays;
            total = (Convert.ToInt32(jum_hari) + 1) * Convert.ToInt32(label15.Text);
            textBox2.Text = total + "";
        }

        private void button4_Click(object sender, EventArgs e)
        {
            //insert ke guest
            try
            {
                OracleCommand oc = new OracleCommand("insert into guest values ('" + textBox3.Text + "','" + textBox4.Text + "','" + textBox5.Text + "','" + textBox6.Text + "')", parent.oc);
                oc.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show("guest" + ex.Message);
            }

            try
            {
                string status = "";

                int total = 0;
                for (int i = 0; i < listTotal.Count; i++)
                {
                    total += Convert.ToInt32(listTotal[i]);
                }
                for (int i = 0; i < listBox2.Items.Count; i++)
                {
                    OracleCommand cmd = new OracleCommand("select harga_fasilitas from fasilitas where nama_fasilitas = '" + listBox2.Items[i] + "'", parent.oc);
                    total += Convert.ToInt32(cmd.ExecuteScalar());
                }
                OracleCommand oc = new OracleCommand("insert into booking values ('" + id_booking + "','" + textBox3.Text + "',to_date('" + dateTimePicker1.Value.ToString("dd-MM-yyyy") + "','dd-mm-yyyy'),to_date('" + dateTimePicker2.Value.ToString("dd-MM-yyyy") + "','dd-mm-yyyy')," + total + ",'DEBT')", parent.oc);
                oc.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show("booking" + ex.Message);
            }

            List<string> listExtra = new List<string>();
            List<int> listJum = new List<int>();
            for (int i = 0; i < listBox2.Items.Count; i++)
            {
                if (!listExtra.Contains(listBox2.Items[i]+""))
                {
                    listExtra.Add(listBox2.Items[i]+"");
                    listJum.Add(1);
                }
                else
                {
                    int a = listExtra.IndexOf(listBox2.Items[i]+"");
                    listJum[a]++;
                }
            }
            for (int j = 0; j < listExtra.Count; j++)
            {
                string id = "";
                try
                {
                    OracleCommand cmd = new OracleCommand("select id_fasilitas from fasilitas where nama_fasilitas='" + listExtra[j] + "'", parent.oc);
                    id = cmd.ExecuteScalar() + "";
                }
                catch (Exception ex)
                {
                    MessageBox.Show("cari id fas" + ex.Message);
                }

                try
                {
                    OracleCommand cmd = new OracleCommand("insert into extra_fasilitas values ('" + id + "','" + id_booking + "','"+listJum[j]+"')", parent.oc);
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("insert extra" + ex.Message);
                }
                

            }

            for (int i = 0; i < listBox3.Items.Count; i++)
            {
                string id_kamar = "";
                try
                {
                    OracleCommand oc = new OracleCommand("select id_kamar from kamar_hotel where nomor_kamar='" + listBox3.Items[i] + "' and id_hotel = '" + parent.cabang + "'", parent.oc);
                    id_kamar = oc.ExecuteScalar() + "";
                }
                catch (Exception ex)
                {
                    MessageBox.Show("id kamar" + ex.Message);
                }

                TimeSpan t = dateTimePicker2.Value - dateTimePicker1.Value;
                double jum_hari = t.TotalDays;

                try
                {
                    OracleCommand oc = new OracleCommand("insert into dbooking values ('" + id_kamar + "','" + id_booking + "','" + listBox3.Items[i] + "','" + Convert.ToInt32(listTotal[i]) + "'," + jum_hari + ")", parent.oc);
                    oc.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("dbooking" + ex.Message);
                }
                
                try
                {
                    OracleCommand cmd = new OracleCommand("update kamar_hotel set status='CLOSED' where nomor_kamar='" + listBox3.Items[i] + "' and id_hotel='" + parent.cabang + "'", parent.oc);
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ganti status kamar" + ex.Message);
                }
            }
            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";
            textBox6.Text = "";
            total = 0;
            textBox2.Text = "0";
            comboBox1.SelectedIndex = 0;
            dataGridView1.ClearSelection();
            refreshHarga();
            refreshData();
            listBox3.Items.Clear();
            MessageBox.Show("Transaksi berhasil!");
            refreshSubtotal();
        }

        private void listBox3_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Test");
        }

        private void listBox3_Click_1(object sender, EventArgs e)
        {
            try
            {
                button1.Hide();
                button6.Show();
                button7.Show();

                int index = listBox3.SelectedIndex;
                
                comboBox1.Select(listTipeKamar[index],1);
                dataGridView1.Rows[listNoKamar[index]].Selected = true;
                string[] a = new string[100];
                a=listExtraFas[index].Split(',');
                listBox2.Items.Clear();
                for (int i = 1; i < 100; i++)
                {
                    if (a != null)
                    {
                        listBox2.Items.Add(a[i]);
                    }
                }
                textBox2.Text = listTotal[index];
            }
            catch (Exception)
            {
                
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            button1.Show();
            button6.Hide();
            button7.Hide();
            total = 0;
            textBox2.Text = "0";
            comboBox1.SelectedIndex = 0;
            dataGridView1.ClearSelection();
            textBox2.Text = "0";
        }

        private void button7_Click(object sender, EventArgs e)
        {
            int idx = listBox3.SelectedIndex;
            listNik.RemoveAt(idx);
            listNama.RemoveAt(idx);
            listEmail.RemoveAt(idx);
            listHp.RemoveAt(idx);
            listTipeKamar.RemoveAt(idx);
            listNoKamar.RemoveAt(idx);
            listExtraFas.RemoveAt(idx);
            listTotal.RemoveAt(idx);
            listBox3.Items.RemoveAt(idx);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            int idx = listBox3.SelectedIndex;

            listNik[idx] = textBox3.Text;
            listNama[idx] = textBox4.Text;
            listEmail[idx] = textBox5.Text;
            listHp[idx] = textBox6.Text;
            listTipeKamar[idx] = comboBox1.SelectedIndex;
            listNoKamar[idx] = dataGridView1.CurrentCell.RowIndex;
            listExtraFas[idx] ="";
            for (int i = 0; i < listBox2.Items.Count; i++)
            {
                listExtraFas[idx]+= "," + listBox2.Items[i].ToString();
            }
            listTotal[idx] = textBox2.Text;
            listBox3.Items.RemoveAt(idx);
            listBox3.Items.Insert(idx, label10.Text);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            groupBox1.Enabled = false;
            groupBox2.Enabled = true;
            groupBox3.Enabled = true;
            groupBox4.Enabled = true;
            panel1.Enabled = true;
            button8.Text = "Cancel";
        }
        private void refreshSubtotal()
        {
            int total = 0;
            for (int i = 0; i < listTotal.Count; i++)
            {
                total += Convert.ToInt32(listTotal[i]);
            }
            for (int i = 0; i < listBox2.Items.Count; i++)
            {
                OracleCommand cmd = new OracleCommand("select harga_fasilitas from fasilitas where nama_fasilitas = '" + listBox2.Items[i] + "'", parent.oc);
                total += Convert.ToInt32(cmd.ExecuteScalar());
            }
            label17.Text = "Subtotal :" + total;
        }
    }
}   
