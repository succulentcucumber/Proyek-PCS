﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace WindowsFormsApp1
{
    public partial class formKamar_add : Form
    {
        public formKamar_add()
        {
            InitializeComponent();
        }

        private void formKamar_add_Load(object sender, EventArgs e)
        {
            this.Dock = DockStyle.Fill;
            Form1 parent = (Form1)this.MdiParent;
            OracleDataAdapter oda = new OracleDataAdapter("SELECT * FROM TIPE_KAMAR",parent.oc);
            DataTable tipeKamar = new DataTable();
            oda.Fill(tipeKamar);
            comboBox1.DataSource = tipeKamar;
            comboBox1.DisplayMember = "JENIS_KAMAR";
            comboBox1.ValueMember = "KODE_TIPE";

            //SET FASILITAS
            String idTipe = comboBox1.SelectedValue.ToString();
            oda = new OracleDataAdapter("SELECT F.NAMA_FASILITAS AS NAMA, I.JML_FASILITAS AS JUMLAH FROM ISI_FASILITAS I, FASILITAS F WHERE I.ID_FASILITAS=F.ID_FASILITAS AND KODE_TIPE='" + idTipe + "'", parent.oc);
            DataTable isifas = new DataTable();
            oda.Fill(isifas);
            dataGridView1.DataSource = isifas;

            //SET NOMOR DEFAULT

            OracleCommand cmd = new OracleCommand("SELECT LPAD(COUNT(*),2,'0') FROM KAMAR_HOTEL WHERE ID_HOTEL='" + parent.cabang + "' AND SUBSTR(NOMOR_KAMAR,1,1)='" + numericUpDown1.Value.ToString() + "'", parent.oc);
            String total = cmd.ExecuteScalar().ToString();
            textBox3.Text = numericUpDown1.Value + total;

            
            //SET ID KAMAR
            cmd = new OracleCommand("SELECT AUTO_GEN_ID_KAMAR('"+parent.cabang+"') FROM DUAL",parent.oc);
            textBox1.Text = cmd.ExecuteScalar().ToString();

            //SET CABANG
            cmd = new OracleCommand("SELECT NAMA_HOTEL FROM CABANG_HOTEL WHERE ID_HOTEL='"+parent.cabang+"'",parent.oc);
            textBox2.Text = cmd.ExecuteScalar().ToString();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Form1 parent = (Form1)this.MdiParent;
            String idTipe = comboBox1.SelectedValue.ToString();
            OracleDataAdapter oda = new OracleDataAdapter("SELECT F.NAMA_FASILITAS AS NAMA, I.JML_FASILITAS AS JUMLAH FROM ISI_FASILITAS I, FASILITAS F WHERE I.ID_FASILITAS=F.ID_FASILITAS AND KODE_TIPE='"+idTipe+"'",parent.oc);
            DataTable isifas = new DataTable();
            oda.Fill(isifas);
            dataGridView1.DataSource = isifas;
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            Form1 parent = (Form1)this.MdiParent;

            OracleCommand cmd = new OracleCommand("SELECT LPAD(COUNT(*),2,'0') FROM KAMAR_HOTEL WHERE ID_HOTEL='"+parent.cabang+"' AND SUBSTR(NOMOR_KAMAR,1,1)='"+numericUpDown1.Value.ToString()+"'", parent.oc);
            String total = cmd.ExecuteScalar().ToString();
            textBox3.Text = numericUpDown1.Value+total;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form1 parent = (Form1)this.MdiParent;
            String id_kamar = textBox1.Text;
            String kode_tipe = comboBox1.SelectedValue.ToString();
            String id_hotel = parent.cabang;
            String nomor_kamar = textBox3.Text;
            OracleCommand cmd = new OracleCommand("INSERT INTO KAMAR_HOTEL VALUES('"+id_kamar+"','"+kode_tipe+"','"+id_hotel+"','"+nomor_kamar+"','OPEN')",parent.oc);
            cmd.ExecuteNonQuery();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
}
