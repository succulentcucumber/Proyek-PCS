﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace WindowsFormsApp1
{
    public partial class formKamar_edit : Form
    {
        public formKamar_edit()
        {
            InitializeComponent();
            
        }

        private void formKamar_edit_Load(object sender, EventArgs e)
        {
            this.Dock = DockStyle.Fill;
            Form1 parent = (Form1)this.MdiParent;
            OracleDataAdapter oda = new OracleDataAdapter("SELECT * FROM TIPE_KAMAR",parent.oc);
            DataTable tipe_kamar = new DataTable();
            oda.Fill(tipe_kamar);
            comboBox1.DataSource = tipe_kamar;
            comboBox1.DisplayMember = "JENIS_KAMAR";
            comboBox1.ValueMember = "KODE_TIPE";

            //SET CABANG
            OracleCommand cmd = new OracleCommand("SELECT NAMA_HOTEL FROM CABANG_HOTEL WHERE ID_HOTEL='" + parent.cabang + "'", parent.oc);
            textBox1.Text = cmd.ExecuteScalar().ToString();
            refreshData();

            //SET ISI FASILITAS
            oda = new OracleDataAdapter("SELECT F.NAMA_FASILITAS AS NAMA, I.JML_FASILITAS AS JUMLAH FROM ISI_FASILITAS I, FASILITAS F WHERE KODE_TIPE='" + comboBox1.SelectedValue.ToString() + "'", parent.oc);
            DataTable isiFas = new DataTable();
            oda.Fill(isiFas);
            dataGridView2.DataSource = isiFas;
        }

        public void refreshData()
        {
            Form1 parent = (Form1)this.MdiParent;
            OracleDataAdapter oda = new OracleDataAdapter("SELECT * FROM KAMAR_HOTEL WHERE ID_HOTEL='"+parent.cabang+"'",parent.oc);
            DataTable kamar_hotel = new DataTable();
            oda.Fill(kamar_hotel);
            dataGridView1.DataSource = kamar_hotel;
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                Form1 parent = (Form1)this.MdiParent;
                int row = e.RowIndex;

                textBox2.Text = dataGridView1[0, row].Value.ToString();
                String id = textBox2.Text;

                OracleCommand cmd = new OracleCommand("SELECT nomor_kamar FROM kamar_hotel WHERE id_kamar = '" + id + "'", parent.oc);
                textBox3.Text = cmd.ExecuteScalar().ToString();
                numericUpDown1.Value = Convert.ToInt32(textBox3.Text.Substring(0, 1));

                cmd = new OracleCommand("SELECT kode_tipe FROM kamar_hotel WHERE id_kamar = '" + id + "'", parent.oc);
                comboBox1.SelectedValue = cmd.ExecuteScalar();
            }
            catch (Exception)
            {}
            
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Form1 parent = (Form1)this.MdiParent;

            OracleDataAdapter oda = new OracleDataAdapter("SELECT F.NAMA_FASILITAS AS NAMA, I.JML_FASILITAS AS JUMLAH FROM ISI_FASILITAS I, FASILITAS F WHERE KODE_TIPE='"+comboBox1.SelectedValue.ToString()+"'", parent.oc);
            DataTable isiFas = new DataTable();
            oda.Fill(isiFas);
            dataGridView2.DataSource = isiFas;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form1 parent = (Form1)this.MdiParent;
            OracleCommand cmd = new OracleCommand("DELETE FROM KAMAR_HOTEL WHERE ID_KAMAR='"+textBox2.Text+"'",parent.oc);
            cmd.ExecuteNonQuery();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form1 parent = (Form1)this.MdiParent;
            String id_kamar = textBox2.Text;
            String kode_tipe = comboBox1.SelectedValue.ToString();
            String nomor_kamar = textBox3.Text;
            OracleCommand cmd = new OracleCommand("UPDATE KAMAR_HOTEL SET KODE_TIPE='"+kode_tipe+"',nomor_kamar='"+nomor_kamar+"' WHERE ID_KAMAR='"+id_kamar+"'",parent.oc);
            cmd.ExecuteNonQuery();
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            Form1 parent = (Form1)this.MdiParent;

            OracleCommand cmd = new OracleCommand("SELECT LPAD(COUNT(*),2,'0') FROM KAMAR_HOTEL WHERE ID_HOTEL='" + parent.cabang + "' AND SUBSTR(NOMOR_KAMAR,1,1)='" + numericUpDown1.Value.ToString() + "'", parent.oc);
            String total = cmd.ExecuteScalar().ToString();
            textBox3.Text = numericUpDown1.Value + total;
        }
    }
}
