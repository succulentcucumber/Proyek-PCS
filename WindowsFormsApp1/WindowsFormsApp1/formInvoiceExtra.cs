﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class formInvoiceExtra : Form
    {
        string idBooking = "";
        public formInvoiceExtra(string id)
        {
            InitializeComponent();
            idBooking = id;
        }

        private void crystalReportViewer1_Load(object sender, EventArgs e)
        {
            invoice_extra invo = new invoice_extra();
            invo.SetDatabaseLogon("pcs_proyek", "5521");
            invo.SetParameterValue("idBooking", idBooking);
            crystalReportViewer1.ReportSource = invo;
            crystalReportViewer1.Refresh();
        }
    }
}
