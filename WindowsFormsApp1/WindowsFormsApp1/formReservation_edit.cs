﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace WindowsFormsApp1
{
    public partial class formReservation_edit : Form
    {
        Form1 parent;
        public formReservation_edit()
        {
            InitializeComponent();
        }

        private void formReservation_edit_Load(object sender, EventArgs e)
        {
            Dock = DockStyle.Fill;
            parent = (Form1)this.MdiParent;

            OracleCommand cmd = new OracleCommand("select nama_hotel from cabang_hotel where id_hotel = '" + parent.cabang + "'", parent.oc);
            label14.Text = cmd.ExecuteScalar() + "";

            refreshBooking();

            //LISTBOX
            OracleDataAdapter oda = new OracleDataAdapter("SELECT * FROM FASILITAS WHERE ADDABLE='YES'", parent.oc);
            DataTable fasilitas = new DataTable();
            oda.Fill(fasilitas);
            listBox1.DataSource = fasilitas;
            listBox1.DisplayMember = "nama_fasilitas";
            listBox2.ValueMember = "id_fasilitas";
        }

        public void refreshBooking()
        {
            parent = (Form1)this.MdiParent;
            DataTable booking = new DataTable();
            OracleDataAdapter oda = new OracleDataAdapter("select * from booking where book_status = 'DEBT' and substr(id_booking,1,3)='"+parent.cabang+"' and id_booking like'%"+textBox1.Text+"%' order by 1", parent.oc);
            oda.Fill(booking);
            dataGridView1.DataSource = booking;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            refreshBooking();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int row = e.RowIndex;
                parent = (Form1)this.MdiParent;
                String nik = dataGridView1[1, row].Value.ToString();
                label2.Text = dataGridView1[0, row].Value.ToString();

                OracleCommand cmd = new OracleCommand("SELECT NAMA FROM GUEST WHERE NIK='" + nik + "'", parent.oc);
                String nama = cmd.ExecuteScalar().ToString();
                cmd = new OracleCommand("SELECT EMAIL FROM GUEST WHERE NIK='" + nik + "'", parent.oc);
                String email = cmd.ExecuteScalar().ToString();
                cmd = new OracleCommand("SELECT NOHP_GUEST FROM GUEST WHERE NIK='" + nik + "'", parent.oc);
                String nohp = cmd.ExecuteScalar().ToString();
                cmd = new OracleCommand("SELECT TOTAL_TRANS FROM BOOKING WHERE ID_BOOKING='"+label2.Text+"'", parent.oc);
                textBox7.Text = cmd.ExecuteScalar().ToString();

                textBox3.Text = nik;
                textBox4.Text = nama;
                textBox5.Text = email;
                textBox6.Text = nohp;

                OracleDataAdapter oda = new OracleDataAdapter("SELECT F.NAMA_FASILITAS AS NAMA, E.JML_EXTRA AS JUMLAH FROM EXTRA_FASILITAS E, FASILITAS F WHERE F.ID_FASILITAS = E.ID_FASILITAS AND E.ID_BOOKING = '" +label2.Text + "'", parent.oc);
                DataTable extraFas = new DataTable();
                oda.Fill(extraFas);
                dataGridView2.DataSource = extraFas;

            }
            catch (Exception)
            { 
            }
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            listBox2.Items.Add(listBox1.Text);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                listBox2.Items.RemoveAt(listBox2.SelectedIndex);
            }
            catch (Exception)
            {

            }
        }

        public void refreshDBook()
        {
            parent = (Form1)this.MdiParent;
            //REFRESH EXTRAFAS
            OracleDataAdapter oda = new OracleDataAdapter("SELECT F.NAMA_FASILITAS AS NAMA, E.JML_EXTRA AS JUMLAH FROM EXTRA_FASILITAS E, FASILITAS F WHERE F.ID_FASILITAS=E.ID_FASILITAS AND E.ID_BOOKING='" +label2.Text + "'", parent.oc);
            DataTable extraFas = new DataTable();
            oda.Fill(extraFas);
            dataGridView2.DataSource = extraFas;

            //UPDATE TOTAL
            OracleCommand cmd = new OracleCommand("SELECT TOTAL_TRANS FROM BOOKING WHERE ID_BOOKING='"+label2.Text+"'", parent.oc);
            textBox7.Text = cmd.ExecuteScalar().ToString();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            parent = (Form1)this.MdiParent;
            for (int i = 0; i < listBox2.Items.Count; i++)
            {
                OracleCommand cmd = new OracleCommand("SELECT COUNT(*) FROM EXTRA_FASILITAS EF, FASILITAS F WHERE F.ID_FASILITAS = EF.ID_FASILITAS AND F.NAMA_FASILITAS='"+listBox2.Items[i].ToString()+"'", parent.oc);
                int totalx = Convert.ToInt32(cmd.ExecuteScalar());
                if (totalx > 0)
                {
                    String idBooking = label2.Text;
                    cmd = new OracleCommand("SELECT EF.JML_EXTRA FROM EXTRA_FASILITAS EF, FASILITAS F WHERE F.ID_FASILITAS = EF.ID_FASILITAS AND F.NAMA_FASILITAS='" + listBox2.Items[i].ToString() + "'", parent.oc);
                    int total = Convert.ToInt32(cmd.ExecuteScalar());
                    total++;
                    cmd = new OracleCommand("SELECT F.ID_FASILITAS FROM EXTRA_FASILITAS EF, FASILITAS F WHERE F.ID_FASILITAS = EF.ID_FASILITAS AND F.NAMA_FASILITAS='" + listBox2.Items[i].ToString() + "'", parent.oc);
                    String idFas = cmd.ExecuteScalar().ToString();
                    cmd = new OracleCommand("UPDATE EXTRA_FASILITAS SET JML_EXTRA="+total+" WHERE ID_BOOKING='"+idBooking+"' AND ID_FASILITAS='"+idFas+"'", parent.oc);
                    cmd.ExecuteNonQuery();
                }
                else
                {
                    cmd = new OracleCommand("SELECT F.ID_FASILITAS FROM FASILITAS F WHERE F.NAMA_FASILITAS='" + listBox2.Items[i].ToString() + "'", parent.oc);
                    String idFas = cmd.ExecuteScalar().ToString();
                    String idBooking = label2.Text;
                    cmd = new OracleCommand("INSERT INTO EXTRA_FASILITAS VALUES('"+idFas+"','"+idBooking+"',1)", parent.oc);
                    cmd.ExecuteNonQuery();
                }
                cmd = new OracleCommand("SELECT HARGA_FASILITAS FROM FASILITAS WHERE NAMA_FASILITAS='"+listBox2.Items[i].ToString()+"'", parent.oc);
                int harga = Convert.ToInt32(cmd.ExecuteScalar());
                cmd = new OracleCommand("SELECT TOTAL_TRANS FROM BOOKING WHERE ID_BOOKING='"+label2.Text+"'", parent.oc);
                int totalt = Convert.ToInt32(cmd.ExecuteScalar());
                totalt += harga;
                cmd = new OracleCommand("UPDATE BOOKING SET TOTAL_TRANS="+totalt+" WHERE ID_BOOKING='"+label2.Text+"'", parent.oc);
                cmd.ExecuteNonQuery();
            }

            MessageBox.Show("Add Facility Success!");
            listBox2.Items.Clear();
            refreshDBook();
            refreshBooking();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            String nama = textBox4.Text;
            String email = textBox5.Text;
            String nik = textBox3.Text;
            String nohp = textBox6.Text;
            Form1 parent = (Form1)this.MdiParent;
            OracleCommand cmd = new OracleCommand("UPDATE GUEST SET NAMA='"+nama+"', EMAIL='"+email+"', NOHP_GUEST='"+nohp+"' WHERE NIK='"+nik+"'", parent.oc);
            cmd.ExecuteNonQuery();
            refreshBooking();
            refreshDBook();

            MessageBox.Show("Berhasil Update Data!");
        }
    }
}
